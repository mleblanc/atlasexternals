# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Package building TBB as part of the offline software build.
#

# Set the package name.
atlas_subdir( TBB )

# In release recompilation mode stop here.
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed.
if( NOT ATLAS_BUILD_TBB )
   return()
endif()

# Tell the user what's happening.
message( STATUS "Building TBB as part of this project" )

# The source package.
set( _source "http://cern.ch/lcgpackages/tarFiles/sources/tbb2018_U1.tar.gz" )
set( _md5 "b2f2fa09adf44a22f4024049907f774b" )

# The temporary directory to set up the built results in.
set( _buildDir "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/TBBBuild" )

# Common flags to give to the "installation script".
set( _scriptArgs -DCMAKE_INSTALL_LIBDIR=${CMAKE_INSTALL_LIBDIR}
   -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
   -DCMAKE_SHARED_LIBRARY_SUFFIX=${CMAKE_SHARED_LIBRARY_SUFFIX}
   -DCMAKE_SYSTEM_PROCESSOR=${CMAKE_SYSTEM_PROCESSOR} )

# Set up the build of TBB for the build area.
ExternalProject_Add( TBB
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL "${_source}"
   URL_MD5 "${_md5}"
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo "No configuration for TBB"
   BUILD_IN_SOURCE 1
   INSTALL_COMMAND ${CMAKE_COMMAND} -DSOURCE_DIR=<SOURCE_DIR>
   -DINSTALL_DIR=${_buildDir} ${_scriptArgs}
   -P ${CMAKE_CURRENT_SOURCE_DIR}/cmake/InstallTBB.cmake
   COMMAND ${CMAKE_COMMAND} -DSOURCE_DIR=<SOURCE_DIR>
   -DINSTALL_DIR=<INSTALL_DIR> ${_scriptArgs}
   -P ${CMAKE_CURRENT_SOURCE_DIR}/cmake/InstallTBB.cmake )
ExternalProject_Add_Step( TBB forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of TBB"
   DEPENDERS download )
add_dependencies( Package_TBB TBB )

# Install TBB.
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
