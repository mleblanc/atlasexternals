Eigen, A C++ Template Library For Linear Algebra
================================================

This package is used to build Eigen for the standalone analysis release.

If an appropriate version of Eigen is already found on the system, then the
package doesn't do anything. Unless you force it to build Eigen no matter what,
using the

    -DATLAS_BUILD_EIGEN:BOOL=TRUE

CMake configuration option.
