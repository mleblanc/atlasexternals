# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Package for building Lhapdf as part of the offline/analysis release.
#

# The package's name:
atlas_subdir( Lhapdf )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# The build needs Boost:
if( ATLAS_BUILD_BOOST )
   set( extra_flags "--with-boost=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}" )
else()
   find_package( Boost 1.41 REQUIRED )
   if( BOOST_LCGROOT )
      set( extra_flags "--with-boost=${BOOST_LCGROOT}" )
   endif()
endif()

# ...and Python:
if( ATLAS_BUILD_PYTHON )
   list( APPEND extra_flags "PYTHON=${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python" )
else()
   find_package( PythonInterp 2.7 REQUIRED )
   find_package( PythonLibs 2.7 REQUIRED )
   list( APPEND extra_flags "PYTHON=${PYTHON_EXECUTABLE}" )
endif()

# The source code of Lhapdf:
set( _lhapdfSource "http://cern.ch/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/" )
set( _lhapdfSource "${_lhapdfSource}/lhapdf-6.1.6-src.tgz" )
set( _lhapdfMd5 "9fddfd257b4a38bbaf1266fa70ef2b4a" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/LhapdfBuild )

# Set up the build of LHAPDF:
ExternalProject_Add( Lhapdf
   PREFIX ${CMAKE_BINARY_DIR}
   URL ${_lhapdfSource}
   URL_MD5 ${_lhapdfMd5}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E chdir 6.1.6 ./configure
   --prefix=${_buildDir} --disable-static ${extra_flags}
   BUILD_IN_SOURCE 1
   BUILD_COMMAND ${CMAKE_COMMAND} -E chdir 6.1.6 make
   COMMAND ${CMAKE_COMMAND} -E chdir 6.1.6 make install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir} <INSTALL_DIR> )
add_dependencies( Package_Lhapdf Lhapdf )
if( ATLAS_BUILD_BOOST )
   add_dependencies( Lhapdf Boost )
endif()
if( ATLAS_BUILD_PYTHON )
   add_dependencies( Lhapdf Python )
endif()

# Install LHAPDF:
install( DIRECTORY ${_buildDir}/
   DESTINATION . OPTIONAL USE_SOURCE_PERMISSIONS )
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/lhapdf-sanitizer.cmake.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/lhapdf-sanitizer.cmake
   @ONLY )
install( SCRIPT
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/lhapdf-sanitizer.cmake )

# The LHAPDF data files:
set( _dataSource "http://cern.ch/atlas-computing/projects/RootCoreExternal" )
set( _dataSource "${_dataSource}/v002/NNPDF23_nlo_as_0118.tar.gz" )
set( _dataMd5 "139d93c4ed265d227294d8f8c72d6ef8" )

# Temporary directory for the data files:
set( _dataDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/LhapdfFiles )

# Download the LHAPDF data files:
ExternalProject_Add( LhapdfFiles
   PREFIX ${CMAKE_BINARY_DIR}
   URL ${_dataSource}
   URL_MD5 ${_dataMd5}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo
   "No configure needed for LhapdfFiles"
   BUILD_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR>/
   ${_dataDir}
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR>/
   <INSTALL_DIR>/share/LHAPDF )
add_dependencies( Package_Lhapdf LhapdfFiles )

# Install the LHAPDF data files:
install( DIRECTORY ${_dataDir}/
   DESTINATION share/LHAPDF OPTIONAL USE_SOURCE_PERMISSIONS )
