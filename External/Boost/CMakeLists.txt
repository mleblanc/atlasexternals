# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building Boost as part of the offline / analysis release.
#

# Set the name of the package:
atlas_subdir( Boost )

# In release recompilation mode stop now:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_BOOST )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Boost as part of this project" )

# The source code of Boost:
set( _boostSource
   "http://cern.ch/lcgpackages/tarFiles/sources/boost_1_66_0.tar.gz" )
set( _boostMd5 "d275cd85b00022313c171f602db59fc5" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/BoostBuild )

# Use all available cores for the build:
atlas_cpu_cores( nCPUs )

# Decide where to take Python from:
if( ATLAS_BUILD_PYTHON )
   set( _extraConf --with-python=${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python )
else()
   find_package( PythonInterp 2.7 REQUIRED )
   find_package( PythonLibs 2.7 REQUIRED )
   set( _extraConf --with-python=${PYTHON_EXECUTABLE} )
endif()

# Only add debug symbols in Debug build mode:
set( _extraOpt )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
   list( APPEND _extraOpt "variant=debug" )
else()
   set( _extraOpt "variant=release" )
endif()
# Set a compiler toolset explicitly. For the cases where multiple compiler
# types are available in the environment at the same time.
if( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" )
   list( APPEND _extraOpt "toolset=clang" )
elseif( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" )
   list( APPEND _extraOpt "toolset=gcc" )
endif()

# Build Boost:
ExternalProject_Add( Boost
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_boostSource}
   URL_MD5 ${_boostMd5}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ./bootstrap.sh --prefix=${_buildDir} ${_extraConf}
   BUILD_COMMAND ./b2 link=shared ${_extraOpt} -j${nCPUs}
   COMMAND ./b2 link=shared ${_extraOpt} install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/
   <INSTALL_DIR> )
add_dependencies( Package_Boost Boost )

# If we are building Python ourselves, make Boost depend on it:
if( ATLAS_BUILD_PYTHON )
   add_dependencies( Boost Python )
endif()

# Install Boost:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
