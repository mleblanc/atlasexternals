nlohmann JSON parser for C++
============================

This package is used to build the nlohmann JSON parser for C++.

The source code can be found here:

<http://github.com/nlohmann/json>

