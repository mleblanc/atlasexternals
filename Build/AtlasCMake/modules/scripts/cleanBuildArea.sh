#!/bin/bash
#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Script to delete stale files from the build area that could be left over from
# a previous build. Typically this happens when source files (or entire packages)
# are removed between incremental builds.
#

# Make sure that we received the right number of arguments:
if [ "$#" -ne 1 ]; then
    echo "Syntax: cleanupBuildArea.sh BINARY_DIR"
    exit 1
fi

binary_dir=$1

# Find list of all .py[c] files
pyc_files=`find $binary_dir/python -name '*.pyc' | sort`
py_files=`find -L $binary_dir/python -name '*.py' -type f | sort`

# Delete .pyc files without corresponding source file
comm -23 --nocheck-order \
     <(echo "$pyc_files") \
     <(echo "$py_files" | sed 's/\.py$/\.pyc/') \
    | xargs rm -f
