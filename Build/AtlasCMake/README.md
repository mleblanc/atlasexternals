Main ATLAS CMake Configuration
==============================

This package collects all the code that takes care of setting up an ATLAS
project build using CMake.

Base projects, like the ones built by this repository, need to include the
code from the package like:

    set( AtlasCMake_DIR ${CMAKE_SOURCE_DIR}.../AtlasCMake )
    find_package( AtlasCMake REQUIRED )

However child projects of these base projects are not expected to include
the AtlasCMake code explicitly like this. They are only expected to find their
base project, which is supposed to take care of setting up the ATLAS CMake
code for them behind the scenes.

The code takes care of setting up all of the compilation/linker flags correctly
for the build, and it provides a whole slew of `atlas_` prefixed functions and
macros that help in setting up the build of ATLAS projects in a consistent
way.
