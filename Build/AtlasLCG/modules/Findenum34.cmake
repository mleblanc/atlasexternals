# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  ENUM34_PYTHON_PATH
#
# Can be steered by ENUM34_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Find it.
lcg_python_external_module( NAME enum34
   PYTHON_NAMES enum/__init__.py enum.py )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( enum34 DEFAULT_MSG
   _ENUM34_PYTHON_PATH )

# Set up the RPM dependency.
lcg_need_rpm( enum34 )
