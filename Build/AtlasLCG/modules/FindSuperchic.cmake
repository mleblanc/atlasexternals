# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Module finding Superchic in the LCG release. Defines:
#  - SUPERCHIC_FOUND
#  - SUPERCHIC_INCLUDE_DIR
#  - SUPERCHIC_INCLUDE_DIRS
#
# Can be steered by SUPERCHIC_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME Superchic
   INCLUDE_NAMES obj/superchic.o
   SEARCH_PATHS ${SUPERCHIC_LCGROOT} )

# Superchic just holds objects. So now set the correct variable for the
# proper environment setting:
set( SUPERCHIC_OBJ_PATH ${SUPERCHIC_INCLUDE_DIRS} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( Superchic DEFAULT_MSG SUPERCHIC_INCLUDE_DIR  )
mark_as_advanced( SUPERCHIC_FOUND SUPERCHIC_INCLUDE_DIR SUPERCHIC_INCLUDE_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( superchic FOUND_NAME SUPERCHIC )

# Set the SUPERCHICPATH environment variable:
if( SUPERCHIC_FOUND )
   set( SUPERCHIC_ENVIRONMENT
      FORCESET SUPERCHICPATH ${SUPERCHIC_LCGROOT} )
endif()

# Unset the include directories. They would just pollute the runtime
# environment.
unset( SUPERCHIC_INCLUDE_DIR )
unset( SUPERCHIC_INCLUDE_DIRS )
