# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  PYANALYSIS_BINARY_PATH
#  PYANALYSIS_PYTHON_PATH
#
# Can be steered by PYANALYSIS_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Dependencies:
find_package( kiwisolver )
find_package( cycler )

# Find pyanalysis.
lcg_python_external_module( NAME pyanalysis
   PYTHON_NAMES site.py isympy.py numpy/__init__.py
   BINARY_NAMES isympy rootpy
   BINARY_SUFFIXES bin )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( pyanalysis DEFAULT_MSG
   _PYANALYSIS_PYTHON_PATH _PYANALYSIS_BINARY_PATH )

# Set up the RPM dependency:
lcg_need_rpm( pyanalysis )
