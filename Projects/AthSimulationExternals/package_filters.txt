# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthSimulationExternals.
#
+ External/CLHEP
+ External/FastJet
+ External/GPerfTools
+ External/Gdb
+ External/Geant4
+ External/GeoModel
+ External/GoogleTest
+ External/HepMCAnalysis
+ External/MKL
+ External/dSFMT
+ External/nlohmann_json
+ External/prmon
+ External/yampl
- .*
