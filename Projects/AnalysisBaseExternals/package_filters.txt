# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AnalysisBaseExternals.
#
+ External/HDF5
+ External/BAT
+ External/Boost
+ External/Davix
+ External/dcap
+ External/Eigen
+ External/lwtnn
+ External/FastJet
+ External/FastJetContrib
+ External/GoogleTest
+ External/HEPUtils
+ External/KLFitter
+ External/Lhapdf
+ External/LibXml2
+ External/MCUtils
+ External/PyAnalysis
+ External/Python
+ External/ROOT
+ External/TBB
+ External/XRootD
- .*
