# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Configuration options for building the AnalysisBase externals. Collected
# into a single place.
#

# Look for appropriate externals:
find_package( PythonInterp 2.7 QUIET )
find_package( PythonLibs 2.7 QUIET )
find_package( Boost 1.58 QUIET )
find_package( Eigen 3.0.5 QUIET )
find_package( ROOT 6.02.12 QUIET )
find_package( Xrootd 4.1 QUIET )
find_package( dcap QUIET )
find_package( Davix QUIET )
find_package( OpenSSL QUIET )
find_package( UUID QUIET )
find_package( TBB 2018 QUIET )

# Get the OS name:
atlas_os_id( _os _osIsValid )

# Decide whether to build Python:
set( _flag FALSE )
if( ( NOT PYTHONINTERP_FOUND ) OR ( NOT PYTHONLIBS_FOUND ) )
   set( _flag TRUE )
elseif( APPLE )
   # For macOS check whether we're picking up the Python version provided
   # by the system.
   get_filename_component( _pythonDir ${PYTHON_EXECUTABLE} DIRECTORY )
   if( ${_pythonDir} STREQUAL "/usr/bin" )
      # Since that's not appropriate for us... :-(
      set( _flag TRUE )
   endif()
   unset( _pythonDir )
endif()
option( ATLAS_BUILD_PYTHON
   "Build Python as part of the release" ${_flag} )

if( ATLAS_BUILD_PYTHON )
   # Make CMake forget that it "found" Python. As it will break the
   # environment setup of the AnalysisBaseExternals project now that
   # we're building Python as a part of it.
   get_property( _packages GLOBAL PROPERTY PACKAGES_FOUND )
   list( REMOVE_ITEM _packages PythonInterp PythonLibs )
   set_property( GLOBAL PROPERTY PACKAGES_FOUND ${_packages} )
   unset( _packages )
endif()
 
# Decide whether to build Boost:
set( _flag FALSE )
if( NOT Boost_FOUND OR ATLAS_BUILD_PYTHON )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_BOOST
   "Build Boost as part of the release" ${_flag} )

# Build Eigen:
option( ATLAS_BUILD_EIGEN
   "Build Eigen as part of the release" TRUE )

# Decide whether to build XRootD:
set( _flag FALSE )
if( NOT XROOTD_FOUND OR ATLAS_BUILD_PYTHON )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_XROOTD
   "Build XRootD as part of the release" ${_flag} )

# Decide whether to build DCAP:
set( _flag FALSE )
if( NOT DCAP_FOUND AND _osIsValid AND
    ( "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" ) AND
    ( ( "${_os}" STREQUAL "slc6" ) OR ( "${_os}" STREQUAL "centos7" ) ) )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_DCAP
   "Build DCAP as part of the release" ${_flag} )

# Decide whether to build LibXml2:
set( _flag TRUE )
if( APPLE )
   set( _flag FALSE )
endif()
option( ATLAS_BUILD_LIBXML2
   "Build LibXml2 as part of the release" ${_flag} )

# Decide whether to build Davix:
set( _flag FALSE )
if( NOT DAVIX_FOUND AND NOT APPLE AND UUID_FOUND AND OPENSSL_FOUND )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_DAVIX
   "Build Davix as part of the release" ${_flag} )

# Decide whether to build TBB:
set( _flag FALSE )
if( NOT TBB_FOUND )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_TBB
   "Build TBB as part of the release" ${_flag} )

# Decide whether to build ROOT:
set( _flag FALSE )
if( NOT ROOT_FOUND OR ATLAS_BUILD_PYTHON OR ATLAS_BUILD_XROOTD
      OR ATLAS_BUILD_DCAP OR ATLAS_BUILD_LIBXML2 OR ATLAS_BUILD_DAVIX
      OR ATLAS_BUILD_TBB )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_ROOT
   "Build ROOT as part of the release" ${_flag} )

# Tell CMake to build PyAnalysis
option( ATLAS_BUILD_PYANALYSIS "Build the PyAnalysis package as part of this project" TRUE )

# Clean up:
unset( _flag )
unset( _os )
unset( _osIsValid )
