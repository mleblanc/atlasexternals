# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# This file is used to configure which externals should be picked up from
# external (AFS/CVMFS) sources.
#

# Find LCG:
set( LCG_VERSION_POSTFIX @LCG_VERSION_POSTFIX@ )
if( AthenaExternals_FIND_QUIETLY )
   find_package( LCG @LCG_VERSION_NUMBER@ REQUIRED EXACT QUIET )
else()
   find_package( LCG @LCG_VERSION_NUMBER@ REQUIRED EXACT )
endif()

# Set the platform name for Gaudi, and the externals:
set( ATLAS_PLATFORM @ATLAS_PLATFORM@ )
set( CMTCONFIG @ATLAS_PLATFORM@ )
set( ENV{CMTCONFIG} ${CMTCONFIG} )

# Add all custom compilation options:
set( CMAKE_CXX_STANDARD @CMAKE_CXX_STANDARD@ CACHE STRING "C++ standard used for the build" )
add_definitions( -DGAUDI_V20_COMPAT )
add_definitions( -DATLAS_GAUDI_V21 )
add_definitions( -DHAVE_GAUDI_PLUGINSVC )

# Allow non-constness for ToolHandles. This is for release 22.
option( ATLAS_ALLOW_TOOLHANDLE_NONCONSTNESS
   "Allow non-const access to tools through const ToolHandle-s" ON )
if( ATLAS_ALLOW_TOOLHANDLE_NONCONSTNESS )
   add_definitions( -DALLOW_TOOLHANDLE_NONCONSTNESS )
endif()

# Use the -pthread flag for the build instead of the -lpthread linker option,
# whenever possible:
set( THREADS_PREFER_PTHREAD_FLAG TRUE CACHE BOOL
   "Prefer using the -pthread compiler flag over -lpthread" )

# Handle CMP0072 by actively selecting the legacy OpenGL library by default.
set( OpenGL_GL_PREFERENCE "LEGACY" CACHE BOOL
   "Prefer using /usr/lib64/libGL.so unless the user says otherwise" )
